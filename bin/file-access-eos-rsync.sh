#!/bin/bash
#

rsync="/usr/bin/rsync"
if [ ! -x $rsync ]
then
    echo ERROR: $rsync not found
    exit 1
fi

# SSH will be used to connect to LXPLUS and there check if the EOS folder exists
ssh="/usr/bin/ssh"
if [ ! -x $ssh ]
then
    echo ERROR: $ssh not found
    exit 1
fi

# Check that the connection to lxplus is possible
$ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes $EOS_ACCOUNT_USERNAME@lxplus.cern.ch 'whoami; echo 0' > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo ERROR: Not possible to connect to lxplus.cern.ch with user \"$EOS_ACCOUNT_USERNAME\"
    exit 1
fi

# Rsync files with EOS
if [ $EOS_GET_OR_DEPLOY = "DEPLOY" ]; then
  if [ $ONE_FILE = "TRUE" ]; then
    echo Deploying file to EOS
    $rsync --verbose --copy-links -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes" $CI_DIR $EOS_ACCOUNT_USERNAME@lxplus.cern.ch:$EOS_PATH
  else
    echo Deploying file\(s\) to EOS
    $rsync --recursive --delete --verbose -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes" $CI_DIR/ $EOS_ACCOUNT_USERNAME@lxplus.cern.ch:$EOS_PATH/
  fi
else
  if [ $ONE_FILE = "TRUE" ]; then
    echo Getting file from EOS
    $rsync --verbose --copy-links -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes" $EOS_ACCOUNT_USERNAME@lxplus.cern.ch:$EOS_PATH $CI_DIR
  else
    echo Getting file\(s\) from EOS
    $rsync --recursive --verbose -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes" $EOS_ACCOUNT_USERNAME@lxplus.cern.ch:$EOS_PATH/ $CI_DIR/
  fi
fi

if [ $? -ne 0 ]
then
    echo ERROR: Rsync to \"$EOS_PATH\" via lxplus.cern.ch, failed
    exit 1
fi
