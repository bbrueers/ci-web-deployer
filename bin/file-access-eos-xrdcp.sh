#!/bin/bash
#

# XROOTD client to copy files to EOS
xrdcp="/usr/bin/xrdcp"
if [ ! -x $xrdcp ]
then
    echo "ERROR: $xrdcp not found"
    exit 1
fi

# Rely in xrootd to do the copy of files to EOS
if [ $EOS_GET_OR_DEPLOY = "DEPLOY" ]; then
  $xrdcp --force --recursive "$CI_DIR/" "$EOS_MGM_URL/$EOS_PATH/" 2>&1 >/dev/null
else
  echo "ERROR: xrdcp not supported for EOS_GET_OR_DEPLOY 'GET'. Please use METHOD 'rsync' instead."
  exit 1
fi
if [ $? -ne 0 ]
then
    echo "ERROR: Failed to copy files from '$CI_DIR/' to '$EOS_PATH' via xrdcp"
    exit 1
fi
